// Task 1:

const student = {
    firstName: "Daniyar",
    lastName: "Izteleuov",
    age: 36,
    married: true,
    children: true,
}

console.log(student);

/*
    Task 1:

    Необходимо создать информацию о себе, используя переменные, в которых будет:
    -ваше имя,
    -ваш возраст,  
    -поле с вашим статусом о семейном положении, замужем/женаты (Либо истина , либо ложь)
    -по аналогии с предыдущим, поле с детьми

    Также необходимо определить тип данных всех ваших полей и вывести результат в консоль
*/
// Task 2:
let firstSide = 23;
let secondSide = 10;
let areaRectangle = firstSide*secondSide;
console.log(areaRectangle);

/* 
    Task 2:

    Напишите скрипт, который находит площадь прямоугольника

    -высота 23см,
    -шириной 10см

    Каждая сущность должна находиться в своей переменной
*/
// Task 3:

let firstHeight = 10;
let areaBase = 4;
let volumeCylinder = firstHeight*areaBase;
console.log(volumeCylinder);
/*
    Task 3:

    Напиши скрипт, который находит объем цилиндра
    
    -высота 10м  
    -площадь основания 4м

    Каждая сущность должна находиться в своей переменной
*/
// Task4

Infinity - "1"
// (string)
// я подумал сперва будет string вышел number
console.log(typeof (Infinity - "1"));
"42" + 42
// (string)
// я подумал сперва будет string также вышло string
console.log(typeof ("42" + 42));
2 + "1 1"
// (string)
// я подумал сперва будет string также вышло string
console.log(typeof (2 + "1 1"));
99 + 101
// (number)
// я подумал сперва будет number также вышло number
console.log(typeof (99 + 101));
"1" - "1"
// (string)
// я подумал сперва будет string также вышло number
console.log(typeof ("1" - "1"));
"Result: " + 10/2
// (string)
// я подумал сперва будет string также вышло string
console.log(typeof ("Result: " + 10/2));
3 + " bananas " + 2 + " apples "
// (string)
// я подумал сперва будет string также вышло string
console.log(typeof (3 + " bananas " + 2 + " apples "));
/*
    Task 4:

    Напиши рядом с каждым выражением , тот ответ который по вашему мнению выведет console.
    И потом сравните ваш результат с тем что на самом деле вывела консоль.
    
    Infinity - "1"
    "42" + 42
    2 + "1 1"
    99 + 101
    "1" - "1"
    "Result: " + 10/2
    3 + " bananas " + 2 + " apples "

*/
